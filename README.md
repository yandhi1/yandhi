Yandhi é um robo agressivo porém sabe ser cauteloso, primeiramente ele faz um escaneamento da area em 360 graus até que encontre um inimigo, no momento que o adversario é definido Yandhi realiza uma investida contra ele.

No momento que o adversario foi escaneado e localizado Yandhi verifica a distancia entre eles dois e avança até chegar em uma distancia especifica para começar a atirar.

Assim que o adversario começa a se mover na direção de Yanhdi ele tenta manter uma distancia segura evitando colisões.
Apartir de uma quantidade especifica de energia do robo inimigo se a energia de Yandhi for menor que a energia do adversario ele começa a ser mais cauteloso.

Se ele for atingido por tiros ele tenta se esquivar se mantendo em movimento, caso o inimigo esteja com a energia baixa Yanhdi entra em frenesi e vai com tudo para cima dele, não se importando muito com colisões porém tentando manter uma pequena distancia do outro robo. 

Caso fique preso ou encostado em alguma parede Yandhi consegue se livrar dessa situação indo para frente, se o inimigo estiver dentro da zona de investida de Yandhi ele prioriza esse movimento e sai da parede ou canto. 

